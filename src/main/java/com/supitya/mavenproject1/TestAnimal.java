/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.mavenproject1;

/**
 *
 * @author ASUS
 */
public class TestAnimal {

    public static void main(String[] args) {
        System.out.println("LandAnimal");
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        Cat c1 = new Cat("Meaw");
        c1.eat();
        c1.sleep();
        c1.walk();
        Dog d1 = new Dog("Hong");
        d1.run();
        d1.eat();
        d1.speak();
        System.out.println("Reptile");
        Crocodile a1 = new Crocodile("Keaw");
        a1.crawl();
        a1.eat();;
        a1.sleep();
        Snakee s1 = new Snakee("Non");
        s1.crawl();
        s1.sleep();
        s1.eat();
        System.out.println("AquaticAnimals");
        Fish f1 = new Fish("Toon");
        f1.swim();
        f1.eat();
        f1.sleep();
        Crab r1 = new Crab("Puu");
        r1.eat();
        r1.swim();
        r1.walk();
        System.out.println("Poultry");
        Bat b1 = new Bat("Dum");
        b1.fly();
        b1.eat();
        b1.sleep();
        Bird b2 = new Bird("Nok");
        b2.eat();
        b2.speak();
        b2.fly();
    }
}
