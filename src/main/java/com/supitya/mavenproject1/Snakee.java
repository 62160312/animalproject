/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.mavenproject1;

/**
 *
 * @author ASUS
 */
public class Snakee extends Reptile {
    private String nickname;
    
     public Snakee (String nickname) {
        super("Snakee",0);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Snakee: "+ nickname + " "+ "crawl");
    }

    @Override
    public void eat() {
         System.out.println("Snakee: "+ nickname + " "+ "eat");
    }

    @Override
    public void walk() {
         System.out.println("Snakee: "+ nickname + " "+ "walk");
    }

    @Override
    public void speak() {
         System.out.println("Snakee: "+ nickname + " "+ "speak");
    }

    @Override
    public void sleep() {
         System.out.println("Snakee: "+ nickname + " "+ "sleep");
    }
    
}
