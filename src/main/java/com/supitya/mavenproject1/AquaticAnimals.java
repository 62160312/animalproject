/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.mavenproject1;

/**
 *
 * @author ASUS
 */
public abstract class AquaticAnimals  extends Animal{
    
    public AquaticAnimals(String name,int numberOfLeg) {
        super(name, 0);
    }
    
    public abstract void swim();
    
    
}
